const { Client } = require('pg')
const client = new Client({
    connectionString: process.env.connectionString
});
client.connect()
    .then(() => {
        console.log("database  connected  successfully");
    })
    .catch(() => {
        console.log("error in database connectivity");
    })
module.exports = {
    //Create Show
    createShow: (request, response) => {
        const state = request.params.state;
        const schema = `CREATE SCHEMA IF NOT EXISTS ${state};`
        client.query(schema, (error, results) => {
            if (error) {
                response.status(500).json({
                    error: error,
                    message: "error in creating schema"
                })
            }
            else {
                const table = `CREATE TABLE ${state}.show(id serial PRIMARY KEY, moviename text,timings time,day date,city varchar(20),fare numeric);`
                client.query(table, (error, results) => {
                    if (error) {
                        response.status(500).json({
                            error: error,
                            message: "error in creating schema"
                        })
                    }
                    else {
                        response.status(201).json(`table created sucessfully created`)
                    }
                })
            }

        })
    },
    //add the show details
    insertShowDetails: (request, response) => {
        const state = request.params.state;
        const { moviename, timings, day, city, fare } = request.body;
        const insertShow = `INSERT INTO ${state}.show (moviename,timings,day,city,fare) VALUES('${moviename}','${timings}','${day}','${city}',${fare})`;
        client.query(insertShow, (error, results) => {
            if (error) {
                response.status(500).json(
                    {
                        message: "Failed to add a show",
                        error: error
                    })
            }
            else {
                response.status(202).json(
                    {
                        message: "Show Added Successfully",
                    })
            }
        })
    },
    //Display the details of a particular show
    displayShow: (request, response) => {
        const state = request.params.state;
        const id = request.params.id;
        client.query(`SELECT * from ${state}.show where id=${id}`, (error, results) => {
            if (error) {
                response.status(500).json({
                    message: "error in get the list of show",
                    error: error
                })
            }
            else {
                response.status(200).json(results.rows);
            }
        })
    },
    //Display the list of shows in a state
    displayShows: (request, response) => {
        const state = request.params.state;
        client.query(`SELECT * from ${state}.show ORDER BY id ASC`, (error, results) => {
            if (error) {
                response.status(500).json({
                    message: "error in get the list of show",
                    error: error
                })
            }
            else {
                response.status(200).json(results.rows);
            }
        })
    },
    //Deletion of the show by Id
    deleteShow: (request, response) => {
        const id = request.params.id;
        const state = request.params.state;
        client.query(`DELETE FROM ${state}.show WHERE id = ${id}`, (error, results) => {
            if (error) {
                console.log("error")
                response.status(500).json({
                    message: "Error in delete of show",
                    error: error
                })
            }
            else {
                response.status(200).json({
                    message: "show deleted sucessfully"
                });
            }
        })
    },
    //Updation show details by Id
    updateShow: (request, response) => {
        const state = request.params.state;
        const id = request.params.id;
        const { moviename, timings, day, city, fare } = request.body;
        client.query(`UPDATE ${state}.show SET moviename = '${moviename}',
        timings='${timings}',day='${day}',city='${city}',fare=${fare} where id=${id}`,
            (error, results) => {
                if (error) {
                    response.status(500).json({
                        message: "error happend while updating a show",
                        error: error
                    })
                }
                else {
                    response.status(200).json({
                        message: "Show updated sucessfully"
                    })
                }
            }
        )
    }
}
