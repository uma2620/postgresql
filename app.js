require('./config/config');
const express = require('express');
const bodyParser = require('body-parser');
const showRouter = require('./routes/shows.routes');
const port = process.env.PORT;

var app = express();

app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)


app.get('/', (request, response) => {
    response.send("movie ticket management");
})


app.use('/api', showRouter);
app.listen(port, () => console.log(`Server started at port : ${port}`))

