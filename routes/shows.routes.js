const express = require('express');
const router = express.Router();
const showsControl =require('../controller/showsControl');

router.post('/shows',showsControl.createShow);
router.post('/shows/:state',showsControl.insertShowDetails);
router.get('/shows/:state',showsControl.displayShows);
router.get('/shows/:state/:id',showsControl.displayShow);
router.put('/shows/:state/:id',showsControl.updateShow);
router.delete('/shows/:state/:id',showsControl.deleteShow);

module.exports = router;
